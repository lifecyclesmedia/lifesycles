import { connect } from 'react-redux';
import { doNothing } from '../redux/actions';
import App from '../components/Homepage';


const mapStateToProps = store => store;

export default connect(
	mapStateToProps,
	{
		doNothing,
	}
)(App);
