import React from 'react';

import { ProgramCard } from '../';
import './styles.scss';

export default () => <div className='programs-block'>
	<h4 className='title'>plans to suit everyone</h4>
	<div className='cards-wrap'>
		<ProgramCard />
		<ProgramCard />
		<ProgramCard />
	</div>
</div>;
