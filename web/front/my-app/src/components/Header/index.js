import React from 'react';
import { Menu } from 'antd';

import './styles.scss';

const MenuItem = Menu.Item;

export default () => <header className='header'>
	<div className='logo-wrap'>
		<img src='https://www.retentionscience.com/wp-content/uploads/2016/05/CustomerLifecycle_775x425_612.png' alt='logo' className='img'/>
	</div>
	<Menu className='menu-wrap' mode="horizontal">
		<MenuItem className='item'>
			<a href='!#' className='title'>Features</a>
		</MenuItem>
		<MenuItem className='item'>
			<a href='!#' className='title'>Form Calendar</a>
		</MenuItem>
		<MenuItem className='item'>
			<a href='!#' className='title'>Demo</a>
		</MenuItem>
		<MenuItem className='item'>
			<a href='!#' className='title'>Log in</a>
		</MenuItem>
		<MenuItem className='item'>
			<a href='!#' className='title'>Sign up</a>
		</MenuItem>
	</Menu>
</header>;
