import React from 'react';
import { Card, Button } from 'antd';

import './styles.scss';

export default () => <div className='benefit-card'>
	<Card bordered={ false } title='easy peasy' className='card-wrap'>
		<h4 className='title'>Hassle-free time tracking</h4>
		<p className='text'>Toggl makes time tracking so simple you’ll actually use it. But even if you forget, our tracking reminders and idle detection have your back.</p>
		<Button className='btn' type='primary'>All features</Button>
	</Card>
</div>;
