import Header from './Header';
import Layout from './Layout';
import Homepage from './Homepage';
import StartSlide from './StartSlide';
import Benefits from './Benefits';
import BenefitCard from './BenefitCard';
import Programs from './Programs';
import ProgramCard from './ProgramCard';
import Demo from './Demo';

export {
	Header,
	Layout,
	Homepage,
	StartSlide,
	Benefits,
	BenefitCard,
	Programs,
	ProgramCard,
	Demo
};
