import React from 'react';
import { Button, Icon } from 'antd';

import './styles.scss';

export default () => <div className='program-wrap'>
	<h4 className='title'>Enterprise</h4>
	<p className='text'>A custom plan for your complex or large organization</p>
	<img 
		src='https://toggl.com/site/images/home/burger@2x-260485b0887600b626a9e2a17b9bb97d.png' 
		alt='default'
		className='img'
	/>
	<Button 
		size='large' 
		shape="round" 
		type='primary'
		className='btn -demo'
	>
		<span className='title'>Contact us</span>
		<Icon className='icon' type="caret-right" />
	</Button>
</div>;
