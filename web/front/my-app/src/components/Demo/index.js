import React from 'react';	
import { Button, Icon } from 'antd';

import './styles.scss';

export default () => <div className='demo-wrap'>
	<h4 className='title'>Try it in our free demo</h4>
	<Button 
		size='large' 
		shape="round" 
		type='primary'
		className='btn -demo'
	>
		<span className='title'>Get started</span>
		<Icon className='icon' type="caret-right" />
	</Button>
</div>;
