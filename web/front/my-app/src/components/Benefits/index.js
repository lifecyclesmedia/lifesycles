import React from 'react';

import { BenefitCard } from '../';
import './styles.scss';

export default () => <div className='benefit-container'>
	<div className='title-wrap'>
		<h3 className='title -main'>A few benefits</h3>
		<h4 className='title -sub'>The simplest time tracker to help you get things done.</h4>
	</div>
	<BenefitCard/>
	<BenefitCard/>
	<BenefitCard/>
</div>;
