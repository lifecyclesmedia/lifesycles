import React from 'react';

import { 
	StartSlide,
	Benefits,
	Programs,
	Demo
} from '../';
import './styles.scss';

export default () => <div className='main-container'>
	<StartSlide/>
	<Benefits/>
	<Demo/>
	<Programs/>
</div>;
