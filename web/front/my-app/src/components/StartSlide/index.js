import React from 'react';
import { Button, Icon } from 'antd';

import './styles.scss';

export default () => <div className='start-wrap'>
	<h1 className='title -main'>Form your own health calendar now!</h1>
	<h3 className='title -sub'>Or try it with our demo</h3>
	<div className='btn-wrap'>
		<Button 
			size='large' 
			shape="round" 
			type='primary'
			className='btn -calendar'
		>
			<Icon className='icon' type="caret-left" />
			<span className='title'>Form Calendar</span>	
		</Button>
		<Button 
			size='large' 
			shape="round" 
			type='primary'
			className='btn -demo'
		>
			<span className='title'>Demo</span>
			<Icon className='icon' type="caret-right" />
		</Button>
	</div>
</div>;
