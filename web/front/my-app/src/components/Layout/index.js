import React from 'react';

import { Header, Homepage } from '../';
import 'antd/dist/antd.css';
import '../../assets/fonts.scss';
import './styles.scss';

export default () => <div className='layout-container'>
	<Header/>
	<main>
		<Homepage/>
	</main>
</div>;

