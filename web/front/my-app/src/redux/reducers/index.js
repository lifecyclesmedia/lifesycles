import { handleActions } from 'redux-actions';

import { doNothing } from '../actions';


export default handleActions({
	[doNothing] (state) {
		return { ...state, nothing: 'loh' };
	},
}, { nothing: 'ne loh' });
