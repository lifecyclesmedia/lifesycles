import { createStore } from 'redux';
import reducers from '../reducers';

/* eslint-disable */
export default createStore(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);