package handler

import (
	"fmt"
	"github.com/gorilla/mux"
	"html/template"
	"log"
	"net/http"
	"os"
)

func Handler() http.Handler {

	r := mux.NewRouter()

	fs := http.FileServer(http.Dir("./web/front/my-app/build/static/"))
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", fs))

	r.HandleFunc("/healthz", healthzHandler)
	r.HandleFunc("/test", testHandler)
	r.HandleFunc("/", indexPageHandler)

	return r
}

func healthzHandler(w http.ResponseWriter, _ *http.Request) {
	fmt.Fprintln(w, "ok")
}

func testHandler(w http.ResponseWriter, _ *http.Request) {
	fmt.Fprintln(w, "ok")
	host, _ := os.Hostname()
	fmt.Fprintf(w, "Version: 1.0.0\n")
	log.Printf("Serving request: %s", host)
}

func indexPageHandler(w http.ResponseWriter, r *http.Request) {

	tmpl, err := template.ParseFiles("./web/front/my-app/build/index.html")
	if err != nil {
		fmt.Println(err)
	}
	tmpl.Execute(w, nil)
}
