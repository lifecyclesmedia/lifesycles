.PHONY: lint build envs deploy

lint:
	gometalinter --enable=misspell --enable=unparam --enable=dupl --enable=gofmt --enable=goimports --disable=gotype --disable=gas --deadline=3m ./...

build:
	GOOS=linux go build -o lifesycles

prepare:
	gcloud auth application-default login
	gcloud config set project flash-district-230210
	gcloud container clusters get-credentials your-first-cluster-1 	 --zone=europe-west3-c
	gcloud docker --authorize-only

deploy:
	docker rmi gcr.io/flash-district-230210/lifesycles-app -f || true
	docker build -t gcr.io/flash-district-230210/lifesycles-app:latest .
	docker push gcr.io/flash-district-230210/lifesycles-app:latest
	kubectl apply -f kubernetes.yaml

