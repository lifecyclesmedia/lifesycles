# Start from golang v1.12 base image
FROM golang:1.12 as builder

# Set the Current Working Directory inside the container
WORKDIR /app

# Add go.mod and go.sum for caching
COPY go.mod .
COPY go.sum .

# Download vendors
RUN go mod download

# Copy everything from the current directory
COPY . .

# Set service name
ARG SERVICE_NAME=life-app

# Build the Go app
RUN CGO_ENABLED=0 \
    GOOS=linux \
    go build \
		-a \
		-installsuffix cgo \
		-o life-app \
		  ./main.go

######## Start a new stage from alpine #######
FROM alpine:latest

RUN apk --no-cache add ca-certificates

WORKDIR /app/

# Copy the Pre-built binary file from the previous stage
COPY --from=builder app/life-app .

COPY  web/ ./web

COPY config.toml .

EXPOSE 8080

ENV PORT 8080

CMD ["./life-app"]


