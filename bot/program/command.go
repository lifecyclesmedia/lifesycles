package program

import (
	tgbotapi "gopkg.in/telegram-bot-api.v4"
	"lifesycles/bot/menu"
)

func UserMapQuery(chatID int64) *tgbotapi.MessageConfig {
	res := tgbotapi.NewMessage(chatID, GetSampleProgram())
	res.ParseMode = "html"
	res.ReplyMarkup = menu.ReturnMenu()
	return &res
}
