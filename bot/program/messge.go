package program

var (
	delimeter      = "---------------------- \n"
	level1Header   = "<strong>План Здоровья</strong> \U0001F4AA \n " + delimeter
	target1Header  = "<strong>Цель дня</strong> \U0001F3AF \n " + delimeter
	day1Header     = "<strong>Весь День</strong> \U0000263C \n  " + delimeter
	morning1Header = "<strong>Утро</strong> \n  " + delimeter
	evening1Header = "<strong>Вечер</strong> \n " + delimeter
	target1        = "-Начать менять себя к лучшему \n "

	day11 = "\U0001F49A Физическая активность \U000026BD \n"
	day12 = "\U0001F49A FMD диета или голодания \U0001F957 \n"

	morning11 = "\U0001F49A Контроль массы тела  \U00002696 \n"

	evening11 = "\U0001F49A Исключить дефицит сна \U0001F4A4 \n"
)

func GetSampleProgram() string {
	return level1Header + target1Header + target1 + day1Header + day11 + day12 + morning1Header + morning11 + evening1Header + evening11
}

/*level2Header = `🙏
	План здоровья \U00002695
🙏🙏🙏\u{1F604}
U+1F602
<b>полужирный</b>, <strong>полужирный</strong>
<i>курсив</i>
<a href="http://www.example.com/">ссылка</a>
<code>строчный моноширинный</code>
<pre>блочный моноширинный (можно писать код)</pre>
<p>&#x1F354</p>
<p>I will display &#129409;</p>
------------------------
    Цель дня🙏
------------------------` */
