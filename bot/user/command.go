package user

import (
	"gopkg.in/telegram-bot-api.v4"
	"lifesycles/bot/menu"
	"log"
)

func ErrorResponseGender(chatID int64, text string) *tgbotapi.MessageConfig {
	res := tgbotapi.NewMessage(chatID, ErrorMessageRu)
	return &res
}

func StartCommandAndGender(chatID int64, text string) *tgbotapi.MessageConfig {
	res := tgbotapi.NewMessage(chatID, RegNewUserRu)
	butt1 := tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("Мужчина", QuerySartAsMan),
	)
	butt2 := tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("Женщина", QuerySartAsWoman),
	)
	butt3 := tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("Не учитывать", QuerySartAsNoting),
	)

	keyb := tgbotapi.NewInlineKeyboardMarkup(butt1, butt2, butt3)
	res.ReplyMarkup = &keyb
	return &res
}

func StartTarget(chatID int64, text string) *tgbotapi.MessageConfig {
	res := tgbotapi.NewMessage(chatID, RegTargetNewUserRu)
	butt1 := tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("Поддержание здоровья \U00002764", QuerySartAsTargetNormal),
	)
	butt2 := tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("Активные изменения \U0001F947", QuerySartAsTargetActive),
	)
	butt3 := tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("Биохакинг \U000026A0", QuerySartAsTargetBiohak),
	)
	keyb := tgbotapi.NewInlineKeyboardMarkup(butt1, butt2, butt3)
	res.ReplyMarkup = &keyb
	return &res
}

func StartAge(chatID int64, text string) *tgbotapi.MessageConfig {
	res := tgbotapi.NewMessage(chatID, RegAgeNewUserRu)
	buttGR1 := tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("20<", QuerySartAsAge20),
		tgbotapi.NewInlineKeyboardButtonData("20-30", QuerySartAsAge2030),
	)
	buttGR2 := tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("30-40", QuerySartAsAge3040),
		tgbotapi.NewInlineKeyboardButtonData("40-55", QuerySartAsAge2030),
	)
	buttGR3 := tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData(">55", QuerySartAsAge20),
	)
	keyb := tgbotapi.NewInlineKeyboardMarkup(buttGR1, buttGR2, buttGR3)
	res.ReplyMarkup = &keyb
	return &res
}

func FinishPreRegistrationMenu(chatID int64, text string) *tgbotapi.MessageConfig {
	log.Println("FinishPreRegistrationMenu")
	res := tgbotapi.NewMessage(chatID, FinishUserRegistrationRu)
	res.ReplyMarkup = menu.MenuKeyboard
	res.ParseMode = "html"
	return &res
}
