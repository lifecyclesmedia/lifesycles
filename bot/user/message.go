package user

var (
	RegNewUserRu = `Привет! Я LifeCycles chatbot. 
Моя главная цель - помочь тебе стать лучше.
Давай знакомиться. Кто ты?`
	RegAgeNewUserRu          = `Здорово! А сколько тебе лет?`
	RegTargetNewUserRu       = `Расскажи чем я могу тебе помочь`
	FinishUserRegistrationRu = `Приятно познакомиться!
Ты всегда можешь уточнить свои ответы в настройках. 
А теперь давай начнем.
Наберите команду
/home - ГЛАВНОЕ МЕНЮ
/sport - СПОРТ
/bcc - ДОБАВКИ
/settings - НАСТРОЙКИ
Или нужный пункт в меню ниже.
`
	QuerySartAsMan          = " /start-as-a-man"
	QuerySartAsWoman        = " /start-as-a-women"
	QuerySartAsNoting       = " /start-as-a-nothing"
	QuerySartAsAge20        = " /start-as-a-20age"
	QuerySartAsAge2030      = " /start-as-a-20-30-age"
	QuerySartAsAge3040      = " /start-as-a-30-40-age"
	QuerySartAsAge4055      = " /start-as-a-40-55-age"
	QuerySartAsAge55        = " /start-as-a-55-age"
	QuerySartAsTargetNormal = " /start-target-normal"
	QuerySartAsTargetActive = " /start-target-active"
	QuerySartAsTargetBiohak = " /start-target-biohack"

	ErrorMessageRu = `Упс... ошибочка, мы её поправим.`
)
