package menu

import tgbotapi "gopkg.in/telegram-bot-api.v4"

var (
	HelpButton = "/help"
	MenuButton = "/home"
)

var MenuKeyboard = tgbotapi.NewReplyKeyboard(
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton(MenuButton),
		tgbotapi.NewKeyboardButton(HelpButton),
	),
)
