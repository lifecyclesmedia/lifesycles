package menu

import (
	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

var ()

func MainMenu() tgbotapi.InlineKeyboardMarkup {
	butt1 := tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData(mapButton, MapButtonQuery),
	)
	butt2 := tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData(todayDietButton, DietMainQuery),
	)
	//butt3 := tgbotapi.NewInlineKeyboardRow(
	//	tgbotapi.NewInlineKeyboardButtonData(bba.BBAButton, "/bba"),
	//)
	butt4 := tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData(settingsDietButton, "/settings"),
		tgbotapi.NewInlineKeyboardButtonData(helpButton, "/help"),
	)
	return tgbotapi.NewInlineKeyboardMarkup(butt1, butt2, butt4)
}

func ReturnMenu() tgbotapi.InlineKeyboardMarkup {
	butt1 := tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData(returnButton, ReturnHomeQuery))

	return tgbotapi.NewInlineKeyboardMarkup(butt1)
}
