package menu

import tgbotapi "gopkg.in/telegram-bot-api.v4"

func DietMainMenu() tgbotapi.InlineKeyboardMarkup {

	butt1 := tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData(dietMy, DietMyQuery),
	)
	butt2 := tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData(dietMyChange, DietChangeQuery),
	)
	butt3 := tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData(returnButton, ReturnHomeQuery),
	)

	return tgbotapi.NewInlineKeyboardMarkup(butt1, butt2, butt3)
}
