package menu

var (
	todayDietButton    = "\U0001F374 Диета на сегодня"
	mapButton          = "\U0001F5FA План здоровья"
	settingsDietButton = "\U0001F513 Настройки"
	helpButton         = "\U00002139 Помощь"
	returnButton       = "\U000023CE Назад"

	//Diet 	===========
	dietMy       = "Моя диета : подробней"
	dietMyChange = "Изменить диету"
)
