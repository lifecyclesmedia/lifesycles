package settings

import (
	"fmt"
	"github.com/BurntSushi/toml"
	"io/ioutil"
	"log"
)

var (
	Database struct {
		Server          string
		LocalDB         string `toml:"local_db"`
		LocalDbPort     string `toml:"local_db_port"`
		LocalServer     string `toml:"local_server"`
		LocalServerPort string `toml:"local_server_port"`
		LocalUser       string `toml:"local_user"`
		LocalPass       string `toml:"local_pass"`
		LocalDev        bool   `toml:"local_dev"`
		ProdDB          string `toml:"prod_db_name"`
		ProdServer      string `toml:"prod_server"`
		ProdServerPort  string `toml:"prod_server_port"`
		ProdUser        string `toml:"prod_user"`
		ProdPass        string `toml:"prod_pass"`
	}
)

type TomlConfig struct {
	DВ dbInfo `toml:"database"`
}

type dbInfo struct {
	Server          string
	LocalDB         string `toml:"local_db"`
	LocalServer     string `toml:"local_server"`
	LocalServerPort string `toml:"local_server_port"`
	LocalUser       string `toml:"local_user"`
	LocalPass       string `toml:"local_pass"`
	LocalDev        bool   `toml:"local_dev"`
	ProdDB          string `toml:"prod_db_name"`
	ProdServer      string `toml:"prod_server"`
	ProdServerPort  string `toml:"prod_server_port"`
	ProdUser        string `toml:"prod_user"`
	ProdPass        string `toml:"prod_pass"`
}

func Init() {
	var config TomlConfig
	if _, err := toml.DecodeFile("config.toml", &config); err != nil {
		log.Println(err)
		files, err := ioutil.ReadDir("./")
		if err != nil {
			log.Fatal(err)
		}

		for _, f := range files {
			fmt.Println(f.Name())
		}
		return
	}
	Database.LocalDev = config.DВ.LocalDev
	Database.LocalServerPort = config.DВ.LocalServerPort
	Database.LocalServer = config.DВ.LocalServer
	Database.LocalUser = config.DВ.LocalUser
	Database.LocalDB = config.DВ.LocalDB
	Database.LocalPass = config.DВ.LocalPass

	Database.ProdServerPort = config.DВ.ProdServer
	Database.ProdServer = config.DВ.LocalServer
	Database.ProdUser = config.DВ.ProdUser
	Database.ProdDB = config.DВ.ProdDB
	Database.ProdPass = config.DВ.ProdPass

}
