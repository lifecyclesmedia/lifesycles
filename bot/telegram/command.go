package telegram

import (
	"errors"
	"gopkg.in/telegram-bot-api.v4"
	"lifesycles/bot/menu"
	"lifesycles/bot/program"
	"lifesycles/bot/user"
	"lifesycles/entity"
	"log"
	//"regexp"
)

var errUserNotFound = errors.New("cannot get user, maybe user is not exist")
var ()

func (b Bot) processMessage(upd tgbotapi.Update) (*tgbotapi.MessageConfig, error) {
	msg := upd.Message
	q := upd.CallbackQuery

	if upd.Message != nil {

		if msg.Text == entity.StartButtonENG {
			return b.startCMD(msg.Chat.ID)
		}
		regMessage, err := b.RegistrationCheck(int64(msg.From.ID), msg.Chat.ID)

		if err != nil {
			return b.errMS(msg.Chat.ID)
		}
		if regMessage != nil {
			return regMessage, nil
		}
		if msg.Text == entity.HelpIcon || msg.Text == entity.HelpButtonENG {
			return b.helpCMD(msg)
		}

		if msg.Text == "/home" {
			msg.MessageID = 0
			return b.mainMenuCMD(msg)
		}

		return b.defaultCMD(msg)
	}
	if q != nil {
		//setGendor
		log.Println(q.Data)
		if q.Data == user.QuerySartAsMan {
			return b.startGenderQR1(q, 0)
		}
		if q.Data == user.QuerySartAsWoman {
			return b.startGenderQR1(q, 1)
		}
		if q.Data == user.QuerySartAsNoting {
			return b.startGenderQR1(q, 2)
		}
		//getAge
		if q.Data == user.QuerySartAsAge20 {
			return b.startAgeQR2(q, 20)
		}
		if q.Data == user.QuerySartAsAge2030 {
			return b.startAgeQR2(q, 30)
		}
		if q.Data == user.QuerySartAsAge3040 {
			return b.startAgeQR2(q, 40)
		}
		if q.Data == user.QuerySartAsAge4055 {
			return b.startAgeQR2(q, 50)
		}
		if q.Data == user.QuerySartAsAge55 {
			return b.startAgeQR2(q, 55)
		}
		//getTarget
		if q.Data == user.QuerySartAsTargetNormal {
			return b.startTargetQR3(q, 0)
		}
		if q.Data == user.QuerySartAsTargetActive {
			return b.startTargetQR3(q, 1)
		}
		if q.Data == user.QuerySartAsTargetBiohak {
			return b.startTargetQR3(q, 2)
		}
		//
		if q.Data == menu.MapButtonQuery {
			return b.mapQR(q)
		}
		if q.Data == menu.ReturnHomeQuery {
			return b.mainMenuQR(q)
		}
		if q.Data == menu.DietMainQuery {
			return b.dietQR(q)
		} else {
			res := tgbotapi.NewMessage(q.Message.Chat.ID, "упс.. в данный момент эта кнопка не работает)")
			return &res, nil
		}

	}

	return nil, nil
}

func (b Bot) errMS(chatID int64) (*tgbotapi.MessageConfig, error) {
	return user.ErrorResponseGender(chatID, simpleErrorMessage), nil
}

func (b Bot) startCMD(chatID int64) (*tgbotapi.MessageConfig, error) {
	return user.StartCommandAndGender(chatID, replyHelp), nil
}

func (b Bot) startGenderQR1(q *tgbotapi.CallbackQuery, gender int8) (*tgbotapi.MessageConfig, error) {
	err := b.UpdateTelegramUserGender(q.Message.Chat.ID, gender)
	if err != nil {
		return b.errMS(q.Message.Chat.ID)
	}
	return user.StartAge(q.Message.Chat.ID, replyHelp), nil
}

func (b Bot) startAgeQR2(q *tgbotapi.CallbackQuery, age int8) (*tgbotapi.MessageConfig, error) {
	err := b.UpdateTelegramUserAge(q.Message.Chat.ID, age)
	if err != nil {
		return b.errMS(q.Message.Chat.ID)
	}
	return user.StartTarget(q.Message.Chat.ID, replyHelp), nil
}

func (b Bot) startTargetQR3(q *tgbotapi.CallbackQuery, target int8) (*tgbotapi.MessageConfig, error) {
	err := b.UpdateTelegramUserTarget(q.Message.Chat.ID, target)
	if err != nil {
		return b.errMS(q.Message.Chat.ID)
	}
	return user.FinishPreRegistrationMenu(q.Message.Chat.ID, replyHelp), nil
}

func (b Bot) mapQR(q *tgbotapi.CallbackQuery) (*tgbotapi.MessageConfig, error) {
	return program.UserMapQuery(q.Message.Chat.ID), nil
}

func (b Bot) dietQR(q *tgbotapi.CallbackQuery) (*tgbotapi.MessageConfig, error) {
	return dietQRReply(q, replyHelp), nil
}
func (b Bot) dietMyQR(q *tgbotapi.CallbackQuery) (*tgbotapi.MessageConfig, error) {
	return dietQRReply(q, replyHelp), nil
}
func (b Bot) mainMenuQR(q *tgbotapi.CallbackQuery) (*tgbotapi.MessageConfig, error) {
	return mainMenuReply(q.Message.Chat.ID, replyBotMainMenu), nil
}
func (b Bot) helpCMD(msg *tgbotapi.Message) (*tgbotapi.MessageConfig, error) {
	return newReply(msg, replyHelp), nil
}
func (b Bot) mainMenuCMD(msg *tgbotapi.Message) (*tgbotapi.MessageConfig, error) {
	return mainMenuReply(msg.Chat.ID, replyBotMainMenu), nil
}

func (b Bot) helpMenuCMD(msg *tgbotapi.Message) (*tgbotapi.MessageConfig, error) {
	return mainMenuReply(msg.Chat.ID, replyHelp), nil
}

func (b Bot) menuClose(msg *tgbotapi.Message) (*tgbotapi.MessageConfig, error) {
	return menuClose(msg, replyHelp), nil
}

func (b Bot) menuCMD(msg *tgbotapi.Message) (*tgbotapi.MessageConfig, error) {
	return menuReply(msg, "main menu"), nil
}

func (b Bot) defaultCMD(_ *tgbotapi.Message) (*tgbotapi.MessageConfig, error) {
	return nil, nil
}
