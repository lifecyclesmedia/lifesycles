package telegram

import (
	"context"
	"fmt"
	"gopkg.in/telegram-bot-api.v4"
	"lifesycles/bot/program"
	"lifesycles/database"
	"log"
)

// Bot is a just lend application wrapper for telegram.
type Bot struct {
	api *tgbotapi.BotAPI
}

// New creates new Bot with chatConfig and transactions, and init tgAPI.
func New(token string, debug bool) (Bot, error) {
	bot, err := tgbotapi.NewBotAPI("694464839:AAE6GULZpxCtkgiy103NRaKBkEfUmMGVVIs")
	if err != nil {
		return Bot{}, fmt.Errorf("could not connect to telegram bot API: %v", err)
	}
	bot.Debug = debug
	log.Printf("Telegram bot authorized on account %s\n", bot.Self.UserName)

	return Bot{api: bot}, nil
}

func (b Bot) Notification() {
	ctx := context.Background()
	users, err := database.TelegramUser.GetTelegramUsersByStatus(ctx, 2)
	if err != nil {
		log.Println(err)
	}
	for _, user := range users {
		msg := tgbotapi.NewMessage(user.ChatID, program.GetSampleProgram())
		msg.ParseMode = "HTML"
		b.api.Send(msg)
	}
}

// Run starts bot and waits for updates messages.
func (b Bot) Run() {
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60
	updates, err := b.api.GetUpdatesChan(u)

	if err != nil {
		log.Printf("could not get updates from chan: %s", err)
		return
	}

	for update := range updates {
		if update.Message == nil && update.CallbackQuery == nil {
			continue
		}
		res, err := b.processMessage(update)
		if err != nil || res == nil {
			log.Printf("could not process message: %v", err)
			continue
		}

		if res != nil {
			if _, err := b.api.Send(res); err != nil {
				log.Printf("could not send message to telegram: %v", err)
			}
		}
	}

	//go func() {
	//	for {
	//		time.Sleep(time.Second * 2)
	//		log.Println("tresreers")
	//		msg := tgbotapi.NewMessage(391460175, "test")
	//		//msg.ReplyToMessageID = update.Message.MessageID
	//		b.api.Send(msg)
	//	}
	//}()

}
