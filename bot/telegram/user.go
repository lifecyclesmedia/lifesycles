package telegram

import (
	"context"
	"gopkg.in/telegram-bot-api.v4"
	"lifesycles/bot/user"
	"lifesycles/database"
	"lifesycles/entity"
	"log"
)

// NewUser creates entity.User from tgbotapi.User.
func NewUser(user tgbotapi.User) entity.User {
	tg := entity.TgUser{
		ID:           int64(user.ID),
		UserName:     user.UserName,
		LanguageCode: user.LanguageCode,
	}
	return entity.User{UserType: entity.TelegramUserType, Tg: &tg}
}

// NewUserWithID creates entity.User  with tgUser.ID.
func NewUserWithID(ID int64) entity.User {
	tg := entity.TgUser{
		ID: ID,
	}
	return entity.User{UserType: entity.TelegramUserType, Tg: &tg}
}

// UpdateTelegramUserGander function for updating user gender
func (b Bot) UpdateTelegramUserGender(userID int64, gender int8) error {
	err := database.TelegramUser.UpdateGenderTelegramUser(userID, gender)
	if err != nil {
		return err
	}
	return database.TelegramUser.UpdateRegStatusTelegramUser(userID, 1)
}

// UpdateTelegramUserTargetfunction for updating user gender
func (b Bot) UpdateTelegramUserTarget(userID int64, target int8) error {
	err := database.TelegramUser.UpdateTargetTelegramUser(userID, target)
	if err != nil {
		return err
	}
	return database.TelegramUser.UpdateRegStatusTelegramUser(userID, 4)
}

// FinishTelegramUserRegistration user registration
func (b Bot) FinishTelegramUserRegistration(userID int64, status int8) error {
	return database.TelegramUser.UpdateRegStatusTelegramUser(userID, status)
}

// UpdateTelegramUserAge function for updating user age
func (b Bot) UpdateTelegramUserAge(userID int64, gender int8) error {
	err := database.TelegramUser.UpdateAgeTelegramUser(userID, gender)
	if err != nil {
		return err
	}
	return database.TelegramUser.UpdateRegStatusTelegramUser(userID, 3)
}

func (b Bot) RegistrationCheck(userID, chatID int64) (*tgbotapi.MessageConfig, error) {
	ctx := context.Background()
	u, err := database.TelegramUser.GetTelegramUserByID(ctx, userID)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	if u == nil {
		u = &entity.TgUser{RegStatus: 0, ChatID: chatID, ID: chatID}
		err = database.TelegramUser.AddTelegramUser(*u)
		if err != nil {
			log.Println(err)
			return nil, err
		}
	}
	if u.RegStatus == 4 {
		return nil, nil
	}
	if u.RegStatus == 0 {
		return user.StartCommandAndGender(chatID, replyHelp), nil
	}
	if u.RegStatus == 1 {
		return user.StartAge(chatID, replyHelp), nil
	}
	if u.RegStatus == 2 {
		return user.StartTarget(chatID, replyHelp), nil
	}
	if u.RegStatus == 3 {
		return user.StartTarget(chatID, replyHelp), nil
		err := b.FinishTelegramUserRegistration(userID, 4)
		if err != nil {
			log.Println(err)
			return nil, err
		}
		return user.FinishPreRegistrationMenu(chatID, replyHelp), nil
	}
	return nil, nil
}

//TODO
func isUserRegitered(userID int64) (int, error) {
	ctx := context.Background()
	u, err := database.TelegramUser.GetTelegramUserByID(ctx, userID)
	if err != nil {
		return 0, nil
	}
	if u == nil {
		return 0, nil
	}
	return 1, nil
}
