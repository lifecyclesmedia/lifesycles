package telegram

import (
	"fmt"
	"strconv"
	"strings"

	"gopkg.in/telegram-bot-api.v4"
	"lifesycles/entity"
)

// parseValue gets last entity ( after last space) and try to convert to float64
// good numbers are "2.2" or "2,2", but bad is "3, 21" ( will convert in 21.00)
func parseValue(msg string) (float64, error) {
	msg = strings.TrimRight(msg, " ")
	li := strings.LastIndex(msg, " ")
	valuestr := strings.Replace(msg[li+1:], ",", ".", 1)
	value, err := strconv.ParseFloat(valuestr, 64)
	if err != nil {
		return 0, fmt.Errorf("could not convert value: %v", err)
	}
	return value, nil
}

// parseUserAndValue returns slice  with user entities, and
// float value, which must be in the string end.
func parseUserAndValue(msg *tgbotapi.Message) ([]entity.User, float64, error) {
	value, err := parseValue(msg.Text)
	if err != nil {
		return nil, 0, err
	}
	users := make([]entity.User, 0)
	for _, e := range *msg.Entities {
		//var username string
		if e.Type == "text_mention" {
			if e.User != nil {
				users = append(users, NewUserWithID(int64((*e.User).ID)))
			}
		}
		if e.Type == "mention" {
			//username = msg.Text[e.Offset+1 : e.Offset+e.Length]
			//users = append(users, entity.User{Tg: &entity.TgUser{UserName: username}})
		}
	}
	return users, value, nil
}
