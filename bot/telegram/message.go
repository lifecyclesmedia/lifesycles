package telegram

import (
	"fmt"
	"gopkg.in/telegram-bot-api.v4"
	"lifesycles/bot/menu"
)

var (
	helpIcon   = "\U00002139 "
	menuIcon   = "\U00002611 "
	helpButton = "help"
	menuButton = " menu"

	replyBotMainMenu = "<strong>Главное Меню</strong>"
	replyDietMenu    = "<strong>Диета Меню</strong>"

	replyHelp = `
it is <strong>LifeSyclesBot</strong> - i will help you
/menu - all information
/help - help data
`
	simpleErrorMessage = `ups...<strong>error happened</strong>`

	replySomeInternalError = `Sorry, service get some internal error. Try later or contact administrator`
)

func formatName(name string, id int) string {
	if id == 0 {
		return fmt.Sprintf("@%s", name)
	}
	return fmt.Sprintf("<a href=\"tg://user?id=%d\">%s</a>", id, name)
}

func dietQRReply(q *tgbotapi.CallbackQuery, text string) *tgbotapi.MessageConfig {
	res := tgbotapi.NewMessage(q.Message.Chat.ID, replyDietMenu)
	res.ParseMode = "html"
	keyb := menu.DietMainMenu()
	res.ReplyMarkup = &keyb
	return &res
}

func newReply(msg *tgbotapi.Message, text string) *tgbotapi.MessageConfig {
	res := tgbotapi.NewMessage(msg.Chat.ID, text)
	res.ReplyToMessageID = msg.MessageID
	res.ParseMode = "html"
	return &res
}
func mainMenuReply(chatID int64, text string) *tgbotapi.MessageConfig {
	res := tgbotapi.NewMessage(chatID, text)
	res.ParseMode = "html"
	keyb := menu.MainMenu()
	res.ReplyMarkup = &keyb
	return &res
}
func menuReply(msg *tgbotapi.Message, text string) *tgbotapi.MessageConfig {
	res := tgbotapi.NewMessage(msg.Chat.ID, replyBotMainMenu)
	res.ParseMode = "html"
	return &res
}

func menuClose(msg *tgbotapi.Message, text string) *tgbotapi.MessageConfig {
	res := tgbotapi.NewMessage(msg.Chat.ID, "")
	res.ParseMode = "html"
	res.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)
	return &res
}

func newMessage(msg *tgbotapi.Message, text string) *tgbotapi.MessageConfig {
	res := tgbotapi.NewMessage(msg.Chat.ID, text)
	res.ParseMode = "html"
	return &res
}
