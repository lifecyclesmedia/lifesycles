package errors

import "errors"

var (
	// ErrChatExists returns chat already exists error.
	ErrChatExists = errors.New("chat already exists")
	// ErrChatNotFound returns chat not found error.
	ErrChatNotFound = errors.New("chat not found")
	// ErrUserNotFound returns user not found error.
	ErrUserNotFound = errors.New("user not found")
)
