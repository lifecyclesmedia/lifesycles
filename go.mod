module lifesycles

go 1.12

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gorilla/mux v1.7.1
	github.com/jasonlvhit/gocron v0.0.0-20190402024347-5bcdd9fcfa9b
	github.com/lib/pq v1.0.0
	github.com/stretchr/testify v1.3.0
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	google.golang.org/appengine v1.5.0 // indirect
	gopkg.in/telegram-bot-api.v4 v4.6.4
)
