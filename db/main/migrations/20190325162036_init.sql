-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE TABLE  users (
  id  SERIAL NOT NULL ,
   firstName  CHARACTER VARYING(35) DEFAULT NULL,
   lastName CHARACTER VARYING(35) DEFAULT NULL,
   password  CHARACTER VARYING(255) DEFAULT NULL,
   roleID  INTEGER DEFAULT NULL,
   timeZone  INTEGER DEFAULT NULL,
   createdAt  INTEGER DEFAULT NULL,
   telegram_users  INTEGER DEFAULT NULL,
  PRIMARY KEY ( id )
) ;

CREATE TABLE  telegram_users  (
     id  SERIAL NOT NULL ,
     chatID  INTEGER NOT NULL,
     age  INTEGER DEFAULT 0,
     gender  INTEGER DEFAULT 0,
     regStage INTEGER DEFAULT 0,
     userName  CHARACTER VARYING(35) DEFAULT NULL,
     languageCode  CHARACTER VARYING(35) DEFAULT NULL,
     createdAt  INTEGER DEFAULT NULL,
PRIMARY KEY ( id )
) ;

CREATE TABLE   rus_bot_messages  (
   id  SERIAL NOT NULL ,
   message  CHARACTER VARYING(255) DEFAULT NULL,
PRIMARY KEY ( id )
) ;


-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE IF EXISTS  users ;
DROP TABLE IF EXISTS  telegram_users ;
DROP TABLE IF EXISTS  rus_bot_messages ;