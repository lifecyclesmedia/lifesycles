-- +goose Up
-- +goose StatementBegin
ALTER TABLE telegram_users  ADD target INTEGER DEFAULT 0 ;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
ALTER TABLE telegram_users DROP target ;
-- +goose StatementEnd
