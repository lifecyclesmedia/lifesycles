#!/bin/sh -e

echo "Applying migrations ..."

./goose-linux64 postgres "postgres://"${DB_USER}":"${DB_USER_PASSWORD}"@"${DB_HOST}":5432/"${DB_NAME}"?sslmode=disable"  up
