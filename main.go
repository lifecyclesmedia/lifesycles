package main

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jasonlvhit/gocron"
	"lifesycles/bot/settings"
	"lifesycles/bot/telegram"
	"lifesycles/database"
	"lifesycles/web/app/handler"
	"log"
	"net/http"
)

func main() {

	log.Println("Config init")
	settings.Init()

	log.Println("Database init")
	database.Init()

	bot, err := setupBot()
	if err != nil {
		log.Fatalf("cannot setup bot: %v", err)
	}

	go bot.Run()
	go func() {
		log.Println("Service started")
		log.Println(http.ListenAndServe(":8080", handler.Handler()))
	}()

	gocron.Every(1).Day().At("05:30").Do(bot.Notification)
	<-gocron.Start()

}

func setupBot() (telegram.Bot, error) {
	return telegram.New("‎694464839:AAE6GULZpxCtkgiy103NRaKBkEfUmMGVVIs", false)

}
