package database

import (
	"context"
	"database/sql"
	"github.com/go-sql-driver/mysql"
	"lifesycles/entity"
	"log"
)

type (
	telegramUser struct{}
)

// AddTelegramUser adds new telegram user.
func (telegramUser) AddTelegramUser(u entity.TgUser) error {
	insForm, err := db.Prepare(` 
 	INSERT INTO telegram_users (
			id,  chatID, age, userName, languageCode
		)
		VALUES ($1, $2, $3, $4, $5) 
		RETURNING id;
 	`)
	if err != nil {
		log.Println(err)
		return err
	}
	defer insForm.Close()
	_, err = insForm.Exec(u.ID, u.ChatID, u.Age, u.UserName, u.LanguageCode)
	me, ok := err.(*mysql.MySQLError)
	if ok {
		log.Println(me.Number)
		return nil
	}
	if err != nil {
		log.Println(err)
		return err
	}
	return err
}

// UpdateRegStatusTelegramUser updates user status.
func (telegramUser) UpdateRegStatusTelegramUser(userID int64, status int8) error {

	updForm, err := db.Prepare(` 
		UPDATE  telegram_users SET regStage = $1 WHERE id=$2 `)
	if err != nil {
		log.Println(err)
		return err
	}
	_, err = updForm.Exec(status, userID)
	if err != nil {
		log.Println(err)
		return err
	}
	return err
}

// UpdateAgeTelegramUser  updates user age.
func (telegramUser) UpdateAgeTelegramUser(userID int64, age int8) error {
	updForm, err := db.Prepare(` 
		UPDATE  telegram_users SET age = $1 WHERE id=$2`)
	if err != nil {
		log.Println(err)
		return err
	}
	_, err = updForm.Exec(age, userID)
	if err != nil {
		log.Println(err)
		return err
	}
	return err
}

// UpdateGenderTelegramUser  updates user age.
func (telegramUser) UpdateGenderTelegramUser(userID int64, gender int8) error {
	updForm, err := db.Prepare(` 
		UPDATE  telegram_users SET gender = $1 WHERE id=$2 `)
	if err != nil {
		log.Println(err)
		return err
	}
	_, err = updForm.Exec(gender, userID)
	if err != nil {
		log.Println(err)
		return err
	}
	return err
}

// UpdateTargetTelegramUser  updates user target.
func (telegramUser) UpdateTargetTelegramUser(userID int64, gender int8) error {
	updForm, err := db.Prepare(` 
		UPDATE  telegram_users SET target = $1 WHERE id=$2 `)
	if err != nil {
		log.Println(err)
		return err
	}
	_, err = updForm.Exec(gender, userID)
	if err != nil {
		log.Println(err)
		return err
	}
	return err
}

// AddTelegramUser checks whether passed emails exist in database and
func (telegramUser) GetTelegramUsers(ctx context.Context) ([]entity.TgUser, error) {
	rows, err := db.Query(`
	SELECT 	
		U.id ,
		U.chatID,
		U.age,
		U.regStage,
		U.userName,
		U.languageCode
	FROM telegram_users AS U `)
	if err != nil {
		return nil, err
	}
	var users []entity.TgUser
	for rows.Next() {
		user := entity.TgUser{}
		err = rows.Scan(
			&user.ID,
			&user.ChatID,
			&user.Age,
			&user.RegStatus,
			&user.UserName,
			&user.LanguageCode)
		if err != nil {
			return nil, err
		}
		users = append(users, user)
	}
	return users, nil
}

// GetTelegramUserByID fetches user by id
func (telegramUser) GetTelegramUserByID(ctx context.Context, id int64) (*entity.TgUser, error) {
	log.Println("GetTelegramUserByID", id)
	row := db.QueryRowContext(ctx, `
SELECT 	
		U.id ,
		U.chatID,
		U.age,
		U.regStage,
		U.userName,
		U.languageCode		
	FROM telegram_users AS U
	   WHERE id = $1`, id)
	user := entity.TgUser{}

	err := row.Scan(
		&user.ID,
		&user.ChatID,
		&user.Age,
		&user.RegStatus,
		&user.UserName,
		&user.LanguageCode)

	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		log.Println(err)
		return nil, err
	}

	return &user, err
}

// GetTelegramUsersByStatus fetches users by status
func (telegramUser) GetTelegramUsersByStatus(ctx context.Context, status int8) ([]entity.TgUser, error) {
	rows, err := db.QueryContext(ctx, `
SELECT 	
		U.id ,
		U.chatID,
		U.age,
		U.regStage,
		U.userName,
		U.languageCode		
	FROM telegram_users AS U
	   WHERE U.regStage = $1`, status)
	users := make([]entity.TgUser, 0)
	for rows.Next() {
		var user entity.TgUser
		err := rows.Scan(
			&user.ID,
			&user.ChatID,
			&user.Age,
			&user.RegStatus,
			&user.UserName,
			&user.LanguageCode)
		if err != nil {
			return nil, err
		}
		users = append(users, user)
	}
	defer rows.Close()

	return users, err
}

// AddTelegramUser adds new telegram user.
func (up telegramUser) DelTelegramUser(ctx context.Context, userID int64) error {
	_, err := db.ExecContext(ctx, `DELETE FROM telegram_users WHERE id = $1`, userID)
	return err
}
