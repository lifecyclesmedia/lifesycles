package database

import (
	"github.com/go-sql-driver/mysql"
	"lifesycles/entity"
	"log"
)

type (
	user struct{}
)

// Add adds new watcher to existing issue.
func (up user) AddNewUser(u entity.TgUser) error {
	log.Println(u)
	insForm, err := db.Prepare(` 
		INSERT INTO users	
		SET id = ?,
		firstName = ?,
		lastName = ?,
		password = ?,
		password = ?,
		roleID = ?,
		createdAt = UNIX_TIMESTAMP()`)
	me, ok := err.(*mysql.MySQLError)
	if ok && me.Number == mysqlKeyExists {
		return nil
	}
	if err != nil {
		log.Println(err)
		return err
	}
	insForm.Exec(u.ID)
	return err
}

func (up user) GetUserByID(userID int64) (*entity.TgUser, error) {
	selDB, err := db.Query("SELECT * FROM dev_life.users WHERE id=?", userID)
	if err != nil {
		return nil, err
	}
	user := entity.TgUser{}
	for selDB.Next() {
		var id, role, timeZone, createdAt *int64
		var firstName, lastName, password *string
		err = selDB.Scan(&id, &firstName, &lastName, &password, &role, &timeZone, &createdAt)
		if err != nil {
			panic(err.Error())
		}
		user.ID = *id

		//user.FirstName = firstName
		//user.LastName = lastName
	}

	defer db.Close()

	return &user, nil

}
