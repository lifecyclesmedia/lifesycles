package database

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"lifesycles/bot/settings"
	"log"
)

const (
	envMysqlInstance = "MYSQL_INSTANCE"
	envMysqlNet      = "MYSQL_NET"
	envMysqlUser     = "MYSQL_USER"
	envMysqlPassword = "MYSQL_PASSWORD"
	envMysqlDBName   = "MYSQL_DB_NAME"
)

var (
	db *sql.DB
	// User is used to access users methods
	User         = user{}
	TelegramUser = telegramUser{}
)

func Init() {
	var err error
	log.Println("DB_INIT")
	if !settings.Database.LocalDev {
		log.Println("Prod DB")
		dbinfo := fmt.Sprintf("user=%s password=%s dbname=%s host=%s sslmode=disable",
			settings.Database.ProdUser, settings.Database.ProdPass, settings.Database.ProdDB, "postgres-main")
		log.Println(dbinfo)
		db, err = sql.Open("postgres", dbinfo)

		if err != nil {
			log.Println(err)
			panic(err.Error())
		}
	} else {
		log.Println("Local DB")
		dbinfo := fmt.Sprintf("user=%s password=%s dbname=%s host=%s sslmode=disable",
			settings.Database.LocalUser, settings.Database.LocalPass, settings.Database.LocalDB, "localhost")
		log.Println(dbinfo)
		db, err = sql.Open("postgres", dbinfo)

		if err != nil {
			log.Println(err)
			panic(err.Error())
		}
	}

	//defer db.Close()

}

func Init_For_Test() {
	var err error
	log.Println("test_DB_INIT")
	dbinfo := fmt.Sprintf("user=%s password=%s dbname=%s  port=%s sslmode=disable",
		"root", "root", "postgres", "5432")
	log.Println(dbinfo)
	db, err = sql.Open("postgres", dbinfo)

	if err != nil {
		log.Println(err)
		panic(err.Error())
	}
}
