package database

import (
	"database/sql"
)

const mysqlKeyExists = 1062

var UserDataProvider UserProvider

type (
	// WatcherProvider implements entity.WatcherProvider to store &
	// access watchers info in mysql database.
	UserProvider struct {
		db *sql.DB
	}
)

// New constructs WatcherProvider object.
func New(db *sql.DB) UserProvider {
	return UserProvider{
		db: db,
	}
}

/*
// GetUser ...

func (up UserProvider) GetUserByID(userID int) (*entity.TgUser, error) {
	selDB, err := up.db.Query("SELECT * FROM users WHERE id=?", userID)
	if err != nil {
		return nil, err
	}
	user := entity.TgUser{}
	for selDB.Next() {
		var id int
		var firstName, lastName string
		err = selDB.Scan(&id, &firstName, &lastName)
		if err != nil {
			panic(err.Error())
		}
		user.ID = id
		user.FirstName = firstName
		user.LastName = lastName
	}

	defer up.db.Close()
	log.Println(user)
	return &user, nil

}
*/
