package database

import (
	"context"
	"github.com/stretchr/testify/assert"
	"lifesycles/entity"
	"testing"
)

func Test_AddTelegramUser(t *testing.T) {
	ctx := context.Background()
	userID := int64(666)
	u := entity.TgUser{
		ID:     userID,
		ChatID: 777,
	}
	err := TelegramUser.AddTelegramUser(u)
	assert.NoError(t, err, "could add user")
	user, err := TelegramUser.GetTelegramUserByID(ctx, userID)
	assert.NoError(t, err, "could get user")
	assert.Equal(t, user.ID, userID)
	err = TelegramUser.DelTelegramUser(ctx, userID)
	assert.NoError(t, err, "could not delete test user")
}
func Test_GetTelegramUsers(t *testing.T) {
	ctx := context.Background()
	userID := int64(66666)
	userID2 := int64(77777)
	u := entity.TgUser{
		ID:     userID,
		ChatID: 7777,
		Age:    1,
	}
	u2 := entity.TgUser{
		ID:     userID2,
		ChatID: 77772,
		Age:    1,
	}
	err := TelegramUser.AddTelegramUser(u)
	assert.NoError(t, err, "could add user1")
	err = TelegramUser.AddTelegramUser(u2)
	assert.NoError(t, err, "could add user2")
	users, err := TelegramUser.GetTelegramUsers(ctx)
	assert.NoError(t, err, "could add user")
	assert.Equal(t, len(users) > 1, true)
	err = TelegramUser.DelTelegramUser(ctx, userID)
	assert.NoError(t, err, "could not delete test user")
	err = TelegramUser.DelTelegramUser(ctx, userID2)
	assert.NoError(t, err, "could not delete test user")
}

func Test_UpdateRegStatusTelegramUser(t *testing.T) {
	ctx := context.Background()
	userID := int64(66666)
	u := entity.TgUser{
		ID:     userID,
		ChatID: 7777,
		Age:    1,
	}

	err := TelegramUser.AddTelegramUser(u)
	assert.NoError(t, err, "could add user1")
	user, err := TelegramUser.GetTelegramUserByID(ctx, userID)
	assert.NoError(t, err, "could get user")
	assert.Equal(t, int8(0), user.RegStatus)
	err = TelegramUser.UpdateRegStatusTelegramUser(3, userID)
	assert.NoError(t, err, "could not update test user")
	user, err = TelegramUser.GetTelegramUserByID(ctx, userID)
	assert.NoError(t, err, "could get user")
	assert.Equal(t, int8(3), user.RegStatus)
	err = TelegramUser.DelTelegramUser(ctx, userID)
	assert.NoError(t, err, "could not delete test user")
}
