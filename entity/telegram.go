package entity

// UserType is user type.
type UserType int

const (
	//UndefinedUserType is for undefined type
	UndefinedUserType UserType = iota
	// TelegramUserType is type of telegram user
	TelegramUserType
)

// TgUser is a user on Telegram. Same as tgbotapi.User
type TgUser struct {
	ID           int64
	ChatID       int64
	Age          int8
	Gender       int8
	RegStatus    int8
	UserName     string // optional
	LanguageCode string // optional
	CreatedAt    int32
	//    IsBot        bool   `bson:"is_bot"`        // optional
}

// String displays a simple text version of a user.
//
// It is normally a user's username, but falls back to a first/last
// name as available.
// Same method as for tgbotapi.User
func (u *TgUser) String() string {
	if u.UserName != "" {
		return u.UserName
	}
	// Todo
	//name := u.FirstName
	//if u.LastName != "" {
	//	name += " " + u.LastName
	//}
	return ""
}

// UserID returns id of internal user
func (u User) UserID() int64 {
	switch u.UserType {
	case TelegramUserType:
		return u.Tg.ID
	case UndefinedUserType:
		return -1
	default:
		return -1
	}
}

// UserRepo defines methods for interaction with users.
type UserRepo interface {
	Add(user TgUser) error
}
