package entity

type (

	// UserName represents user full name
	UserName struct {
		FirstName string `json:"first_name"`
		LastName  string `json:"last_name"`
	}

	// User has user structure
	User struct {
		ID int64 `json:"-"`
		UserName
		Email        string `json:"email"`
		TimeZone     string `json:"-"`
		Role         string `json:"-"`
		PasswordHash string `json:"-"`
		IsRegistered bool   `json:"-"`
		TgID         string
		UserType     UserType
		Tg           *TgUser
	}

	// LoginCredentials is struct with email and password
	LoginCredentials struct {
		Email
		Password string `json:"password"`
	}

	// Email represents request body for only email request
	Email struct {
		UserID            int64  `json:"-"`
		Email             string `json:"email"`
		IsVerified        bool   `json:"-"`
		VerificationToken string `json:"-"`
	}
)
